# darkest_to_json

DEPRECATED

Use darkest_dungeon_tools instead.

A tool to parse .darkest files from the game 'Darkest Dungeon' into a sensible JSON interpretation.

## Functions

`darkest_to_json.convert(_content: string) -> Object`

Parses a string containing darkest data.

`darkest_to_json.sync(_file: string / Buffer) -> Object`

Parses a file synchronously.

`darkest_to_json.async(_file: string / Buffer, _callback: (_err, _Object) -> void) -> Object`

Parses a file asynchronously.