#!/usr/bin/env node

var dtj;

try
{
    dtj = require(".");
} catch(e)
{
    
    dtj = require("darkest_to_json");
}

var fs = require("fs");
var process = require("process");

if(process.argv.length < 3)
{
    console.log("Not enough arguments!\nUsage: cli IN_FILE [OUT_FILE]\n");
    return 0;
}

var inFile = process.argv[2];
var outFile;

if(process.argv.length > 3)
{
    outFile = process.argv[3];
}
else
{
    outFile = inFile + ".json";
}

fs.writeFileSync(outFile, JSON.stringify(dtj.sync(inFile), null, 2));
